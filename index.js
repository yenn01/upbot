const fs = require('fs');
const Discord = require('discord.js');
const { prefix } = require('./config.json');
var { pingDatabase, checkExpiry } = require('./functions/database');
var { pingStorage } = require('./functions/storage');

var testSecrets;
try {
	testSecrets = require(`./secrets.json`);
	console.log("Testing");
} catch(error) {
	console.log("Production");
}

pingDatabase();
pingStorage();

const client = new Discord.Client();
client.commands = new Discord.Collection();
const cooldowns = new Discord.Collection();

setInterval(async () => {
	var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'https://upbot-cloud.herokuapp.com/');
	xhr.send();
	await checkExpiry();
}, 1800000);
	
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	
	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.name, command);
}
	
client.once('ready', () => {
	console.log('Discord Bot is ready.');
});
	
client.on('message', async message => {
		
	if (!message.content.startsWith(prefix) || message.author.bot) return; //If text doesnt start with prefix or written by bot, end.
	
	//used to interpret the text sent to the bot.
	const args = message.content.slice(prefix.length).split(/ +/);
	const commandName = args.shift().toLowerCase();
	console.log("----------------------------------------------\n",`Command called by ${message.author.id}:`, commandName, "\n----------------------------------------------");
	if (!client.commands.has(commandName)) return;
	
	const command = client.commands.get(commandName);
	
	if (command.args && !args.length) {
		let reply =  `You didn't provide any arguments, ${message.author}!`;
	
		if (command.usage) {
			reply += `\nThe proper usage would be: \`${prefix}${command.name} ${command.usage}\``;
		}
	
		return message.channel.send(reply);
	}
	if (!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}
	
	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 3) * 1000;
	
	if (timestamps.has(message.author.id)) {
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.reply(`Please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
		}
	}
	
	timestamps.set(message.author.id, now);
	setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
	
	try {
		command.execute(message, args);
	} catch (error) {
		console.error(error);
		message.reply('There was an error trying to execute that command!');
	}
	
});
	
client.login(process.env.BOT_TOKEN || testSecrets.devBotToken);
