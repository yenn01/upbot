const fs = require('fs');
var BoxSDK = require('box-node-sdk');
var { checkRecord, recordFileKey, recordFileId, getFileKey, getFileId } = require('../functions/database');

var testSecrets;
try {
	testSecrets = require(`../secrets.json`);
} catch(error) {}

var privatekey2;
if (process.env.private_key) {
	privatekey2 = process.env.appAuthPrivateKey.replace(/\\n/g, '\n');
}

var sdk = new BoxSDK({
	clientID: process.env.clientIDBox || testSecrets.clientIDBox,
	clientSecret: process.env.clientSecretBox || testSecrets.clientSecretBox,
	appAuth: {
		keyID: process.env.appAuthKeyID || testSecrets.appAuthKeyID,
		privateKey: privatekey2 || testSecrets.appAuthPrivateKey,
		passphrase: process.env.appAuthPassphrase || testSecrets.appAuthPassphrase
	}
});

var BoxClient = sdk.getAppAuthClient('enterprise', process.env.developerTokenBox || testSecrets.developerTokenBox);

module.exports = {
    pingStorage: () => {
        BoxClient.users.get(BoxClient.CURRENT_USER_ID)
            .then(user => console.log('From storage:', user.name + '!'))
            .catch(err => console.log('From storage:', err));
	},
	uploadToStorage,
	generateFileKey,
	generateDownloadLink,
	deleteFile
}

async function uploadToStorage(key, file, fileKey) {
	return new Promise(async (resolve) => {
		let checkKey = await checkRecord(key);
		if (checkKey !== false) {
			if (await recordFileKey(checkKey, fileKey, file)) {
				BoxClient.files.uploadFile('0', file.filename, fs.createReadStream(file.path),function(err, file) {
					if (err) {
						console.log('Error when uploading file: ' + err);
					} else {
						console.log('File uploaded.');
					}
				}).then(details => {
					BoxClient.files.getDownloadURL(details.entries[0].id).then(async downloadURL => {
						if (await recordFileId(fileKey, details.entries[0].id)) {
							resolve(true);
						}
					});
				});
			}
		} else {
			resolve(false);
		}
	});
}

async function generateDownloadLink(key) {
	return new Promise(async (resolve) => {
		let fileKey = await getFileKey(key);
		let fileId = await getFileId(fileKey);
		BoxClient.files.getDownloadURL(fileId).then(downloadURL => {
			resolve(downloadURL);
		});
	});
}

function generateFileKey() {
	var length = 30;
	var result = '';
	var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var charactersLength = characters.length;
	for ( var i = 0; i < length; i++ ) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

async function deleteFile(key) {
	let fileKey = await getFileKey(key);
	let fileId = await getFileId(fileKey);
	return new Promise((resolve) => {
		BoxClient.files.delete(fileId)
		.then((response) => {
			console.log(response);
			resolve(response);
		})
		.catch((error) => {
			console.log(error);
			resolve(error);
		});
	});
}